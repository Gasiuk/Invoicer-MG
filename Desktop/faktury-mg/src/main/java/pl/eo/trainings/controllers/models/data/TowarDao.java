package pl.eo.trainings.controllers.models.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.eo.trainings.controllers.models.Towar;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface TowarDao extends CrudRepository<Towar, Integer> {
}
