package pl.eo.trainings.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.eo.trainings.controllers.models.Category;
import pl.eo.trainings.controllers.models.Cennik;
import pl.eo.trainings.controllers.models.data.CategoryDao;
import pl.eo.trainings.controllers.models.data.CennikDao;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("cennik")
public class CennikController {

    @Autowired
    CennikDao cennikDao;

    @Autowired
    CategoryDao categoryDao;

    @RequestMapping(value = "")
    public String index(Model model) {

        model.addAttribute("cenniki", cennikDao.findAll());
        model.addAttribute("title", "Moje Cenniki");
        return "cennik/index";
    }

    @RequestMapping(value = "dodajCennik", method = RequestMethod.GET)
    public String wyswietlFormularzDodawaniaCennika(Model model) {
        model.addAttribute("title", "Dodaj Cennik");
        model.addAttribute(new Cennik());
        model.addAttribute("categories", categoryDao.findAll());
        return "cennik/dodajCennik";

    }
    @RequestMapping(value = "dodajCennik", method = RequestMethod.POST)
    public String przetworzFormularzDodawaniaCennika(@ModelAttribute @Valid Cennik newCennik,
                                                     Errors errors, @RequestParam int categoryId, Model model) {

        if (errors.hasErrors()) {
            model.addAttribute("title", "Dodaj Cennik");
            model.addAttribute("categories", categoryDao.findAll());
            return "cennik/dodajCennik";
        }

        Category cat = categoryDao.findOne(categoryId);
        newCennik.setCategory(cat);
        cennikDao.save(newCennik);
        cennikDao.save(newCennik);
        return "redirect:";

    }

    @RequestMapping(value = "usunCennik", method = RequestMethod.GET)
    public String displayRemoveCennikForm(Model model) {
        model.addAttribute("cenniki", cennikDao.findAll());
        model.addAttribute("title", "Usuń Cennik");
        return "cennik/usunCennik";
    }

    @RequestMapping(value = "usunCennik", method = RequestMethod.POST)
    public String processRemoveCennikForm(@RequestParam int[] cennikIds) {

        for (int cennikId : cennikIds) {
            cennikDao.delete(cennikId);
        }

        return "redirect:";
    }

    @RequestMapping(value = "category", method = RequestMethod.GET)
    public String category (Model model, @RequestParam int id) {

        Category cat = categoryDao.findOne(id);
        List<Cennik> cenniki = cat.getCenniki();
        model.addAttribute("cenniki", cenniki);
        model.addAttribute("title", "Cena w kategorii: " + cat.getName());
        return "cennik/index";

    }
}
