package pl.eo.trainings.controllers.models;

public enum TowarType {

    KG ("KG"),
    SZT ("SZT"),
    M ("M"),
    LITR ("LITR"),
    ZESTAW ("ZESTAW");

    private final String name;

    TowarType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
