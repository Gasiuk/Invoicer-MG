package pl.eo.trainings.controllers.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Klient {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    private String nazwa;

    @NotNull
    private String imie;

    @NotNull
    private String nazwisko;

    @NotNull
    private String adres;

    @NotNull
    private String nip;

    private KlientType type;

    public Klient (String nazwa, String imie, String nazwisko, String adres, String nip) {
        this.nazwa = nazwa;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.adres = adres;
        this.nip = nip;
    }

    public Klient() { }

    public int getId() {
        return id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }


    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public KlientType getType() {
        return type;
    }

    public void setType(KlientType type) {
        this.type = type;
    }
}
