package pl.eo.trainings.controllers.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Towar {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    @Size(min=3, max=15)
    private String nazwa;

    @NotNull
    private String kategoria;

    @NotNull
    @Size(min=1, message = "To pole nie może być puste")
    private String description;

    private TowarType type;

    public Towar (String nazwa, String description, String kategoria) {
        this.nazwa = nazwa;
        this.kategoria = kategoria;
        this.description = description;
    }

    public Towar() { }

    public int getId() {
        return id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }


    public String getKategoria() {
        return kategoria;
    }

    public void setKategoria(String kategoria) {
        this.kategoria = kategoria;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TowarType getType() {
        return type;
    }

    public void setType(TowarType type) {
        this.type = type;
    }
}
