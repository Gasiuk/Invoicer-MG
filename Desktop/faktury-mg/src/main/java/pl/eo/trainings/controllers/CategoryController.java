package pl.eo.trainings.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.eo.trainings.controllers.models.Category;

import pl.eo.trainings.controllers.models.data.CategoryDao;

import javax.validation.Valid;
import java.util.ArrayList;

@Controller
@RequestMapping("category")
public class CategoryController {

	@Autowired
	private CategoryDao categoryDao;

	static ArrayList<String> categories = new ArrayList<>();

	@RequestMapping(value = "")
	public String index(Model model) {

		model.addAttribute("categories", categoryDao.findAll());
		model.addAttribute("title", "Moje Kategorie");
		return "categories/index";
	}
	//FORMULARZ ROZPOCZYNA SIE TUTAJ
	@RequestMapping(value = "dodajCategory", method = RequestMethod.GET)
	public String wyswietlFormularzDodawaniaCategory(Model model) {
		model.addAttribute("title", "Dodaj Category");
		model.addAttribute(new Category());

		return "category/dodajCategory";

	}
	//PRZETWARZANIE FORMULARZA
	@RequestMapping(value = "dodajCategory", method = RequestMethod.POST)
	public String przetworzFormularzDodawaniaCategory(@ModelAttribute @Valid Category newCategory,
													Errors errors, Model model) {

		if (errors.hasErrors()) {
			model.addAttribute("title", "Dodaj Kategorie");
			return "category/dodajCategory";
		}

		categoryDao.save(newCategory);
		return "redirect:";

	}

	@RequestMapping(value = "usunCategory", method = RequestMethod.GET)
	public String displayRemoveCategoryForm(Model model) {
		model.addAttribute("categories", categoryDao.findAll());
		model.addAttribute("title", "Usuń Categories");
		return "categories/usunCategories";
	}

	@RequestMapping(value = "usunCategories", method = RequestMethod.POST)
	public String processRemoveCategoryForm(@RequestParam int[] categoryIds) {

		for (int categoryId : categoryIds) {
			categoryDao.delete(categoryId);
		}

		return "redirect:";
	}

}
