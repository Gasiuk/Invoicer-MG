package pl.eo.trainings.controllers.models;

public enum CennikType {

    PIERWSZY ("KLAWIATURA TESTOWA"),
    DRUGI ("KOMPUTER TESTOWY"),
    TRZECI ("MYSZKA TESTOWA");

    private final String name;

    CennikType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
