package pl.eo.trainings.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.eo.trainings.controllers.models.Klient;
import pl.eo.trainings.controllers.models.KlientType;
import pl.eo.trainings.controllers.models.data.KlientDao;

import javax.validation.Valid;
import java.util.ArrayList;

@Controller
@RequestMapping("klient")
public class KlientController {

    @Autowired
    private KlientDao klientDao;

    static ArrayList<String> klienci = new ArrayList<>();

    @RequestMapping(value = "")
    public String index(Model model) {

        model.addAttribute("klienci", klientDao.findAll());
        model.addAttribute("title", "Moi Klienci");
        return "klient/index";
    }
    //FORMULARZ ROZPOCZYNA SIE TUTAJ
    @RequestMapping(value = "dodajKlient", method = RequestMethod.GET)
    public String wyswietlFormularzDodawaniaKlienta(Model model) {
        model.addAttribute("title", "Dodaj Klienta");
        model.addAttribute(new Klient());
        model.addAttribute("klientTypes", KlientType.values());
        return "klient/dodajKlient";

    }
    //PRZETWARZANIE FORMULARZA
    @RequestMapping(value = "dodajKlient", method = RequestMethod.POST)
    public String przetworzFormularzDodawaniaKlienta(@ModelAttribute @Valid Klient newKlient,
                                                     Errors errors, Model model) {

        if (errors.hasErrors()) {
            model.addAttribute("title", "Dodaj Klienta");
            return "klient/dodajKlient";
        }

        klientDao.save(newKlient);
        return "redirect:";

    }

    @RequestMapping(value = "usunKlient", method = RequestMethod.GET)
    public String displayRemoveKlientForm(Model model) {
        model.addAttribute("klienci", klientDao.findAll());
        model.addAttribute("title", "Usuń Klienta");
        return "klient/usunKlient";
    }

    @RequestMapping(value = "usunKlient", method = RequestMethod.POST)
    public String processRemoveKlientForm(@RequestParam int[] klientIds) {

        for (int klientId : klientIds) {
            klientDao.delete(klientId);
        }

        return "redirect:";
    }

}
