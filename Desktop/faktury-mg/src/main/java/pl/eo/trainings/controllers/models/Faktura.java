package pl.eo.trainings.controllers.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;


@Entity
public class Faktura {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    private String nazwa;

    @NotNull
    private String imie;

    @NotNull
    private String nazwisko;

    @NotNull
    private String adres;

    @NotNull
    private String nip;

    @NotNull
    private String dataWystawienia;

    @NotNull
    private String dataDostarczenia;

    @NotNull
//    @Size(min=1, message = "To pole nie może być puste")
    private String description;

    private FakturaType type;

    public Faktura(String nazwa, String description, String imie, String nazwisko, String adres, String nip, String dataWystawienia, String dataDostarczenia, String type2) {
        this.nazwa = nazwa;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.adres = adres;
        this.nip = nip;
        this.dataWystawienia = dataWystawienia;
        this.dataDostarczenia = dataDostarczenia;
        this.description = description;

    }

    public Faktura() { }

    public int getId() {
        return id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getDataWystawienia() {
        return dataWystawienia;
    }

    public void setDataWystawienia(String dataWystawienia) {
        this.dataWystawienia = dataWystawienia;
    }

    public String getDataDostarczenia() {
        return dataDostarczenia;
    }

    public void setDataDostarczenia(String dataDostarczenia) {
        this.dataDostarczenia = dataDostarczenia;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FakturaType getType() {
        return type;
    }

    public void setType(FakturaType type) {
        this.type = type;
    }

}
