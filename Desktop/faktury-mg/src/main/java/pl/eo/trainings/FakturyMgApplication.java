package pl.eo.trainings;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FakturyMgApplication {

	public static void main(String[] args) {
		SpringApplication.run(FakturyMgApplication.class, args);
	}
}
