package pl.eo.trainings.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import pl.eo.trainings.controllers.models.Cennik;
import pl.eo.trainings.controllers.models.Menu;
import pl.eo.trainings.controllers.models.data.CennikDao;
import pl.eo.trainings.controllers.models.data.MenuDao;
import pl.eo.trainings.controllers.models.form.AddMenuItemForm;

import javax.validation.Valid;

@Controller
@RequestMapping("menu")
public class MenuController {

	@Autowired
	private MenuDao menuDao;

	@Autowired
	private CennikDao cennikDao;

	@RequestMapping(value = "")
	public String index(Model model) {
		model.addAttribute("title", "Menus");
		model.addAttribute("menus", menuDao.findAll());
		return "menu/index";
	}

	//FORMULARZ ROZPOCZYNA SIE TUTAJ
	@RequestMapping(value = "dodajMenu", method = RequestMethod.GET)
	public String wyswietlFormularzDodawaniaMenu(Model model) {
		model.addAttribute("title", "Dodaj Menu");
		model.addAttribute(new Menu());
		return "menu/dodajMenu";

	}

	//PRZETWARZANIE FORMULARZA
	@RequestMapping(value = "dodajMenu", method = RequestMethod.POST)
	public String przetworzFormularzDodawaniaMenu(@ModelAttribute @Valid Menu newMenu,
												  Errors errors, Model model) {

		if (errors.hasErrors()) {
			model.addAttribute("title", "Dodaj Menu");
			return "menu/dodajMenu";
		}

		menuDao.save(newMenu);

		return "redirect:viewMenu/" + newMenu.getId();
	}

	@RequestMapping(value = "viewMenu/{menuId}", method = RequestMethod.GET)
	public String viewMenu(Model model, @PathVariable int menuId) {

		Menu menu = menuDao.findOne(menuId);
		model.addAttribute("title", menu.getName());
		model.addAttribute("cenniki", menu.getCenniki());
		model.addAttribute("menuId", menu.getId());

		return "menu/viewMenu";
	}

	@RequestMapping(value = "add-itemMenu/{menuId}", method = RequestMethod.GET)
	public String addItem(Model model, @PathVariable int menuId) {

		Menu menu = menuDao.findOne(menuId);

		AddMenuItemForm form = new AddMenuItemForm(cennikDao.findAll(), menu);

		model.addAttribute("title", "Add item to menu: " + menu.getName());
		model.addAttribute("form", form);
		return "menu/add-itemMenu";

	}

	@RequestMapping(value = "add-itemMenu", method = RequestMethod.POST)
	public String addItem(Model model, @ModelAttribute @Valid AddMenuItemForm form, Error errors) {

//		if (errors.hasErrors()) {
//			model.addAttribute("form", form);
//			return "menu/add-itemMenu";
//		}

		Cennik theCennik = cennikDao.findOne(form.getCennikId());
		Menu theMenu = menuDao.findOne(form.getMenuId());
		theMenu.addItem(theCennik);
		menuDao.save(theMenu);

		return "redirect:/menu/view/" + theMenu.getId();

	}
}
