package pl.eo.trainings.controllers.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;


@Entity
public class Cennik {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    private String nazwat;

    @NotNull
    private String cena;

    // cennik może posiadać tylko jedną kategorię
    @ManyToOne
    private Category category;

//    Hibernate określa mapowanie dla tej konkretnej listy, property cenniki, klasa Menu
    @ManyToMany(mappedBy = "cenniki")
    private List<Menu> menus;

    public Cennik(String nazwat, String cena) {

        this.nazwat = nazwat;
        this.cena = cena;
    }

    public Cennik() { }

    public int getId() {
        return id;
    }

    public String getNazwat() {
        return nazwat;
    }

    public void setNazwat(String nazwa) {
        this.nazwat = nazwa;
    }

    public String getCena() {
        return cena;
    }

    public void setCena(String cena) {
        this.cena = cena;
    }


    public void setCategory(Category category) {
        this.category = category;
    }

}
