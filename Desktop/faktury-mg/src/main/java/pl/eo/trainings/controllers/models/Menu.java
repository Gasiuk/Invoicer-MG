package pl.eo.trainings.controllers.models;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Menu {

	@Id
	@GeneratedValue
	private int id;

	@NotNull
	@Size (min=1, max=50)
	private String name;

	@ManyToMany
	private List<Cennik> cenniki;

	public Menu() {}

	public void addItem(Cennik item) {cenniki.add(item);}

	public int getId() { return id;}

	public String getName() { return name;}

	public void setName(String name) { this.name = name;}

	public List<Cennik> getCenniki() {return cenniki; }
}
