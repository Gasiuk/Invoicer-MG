package pl.eo.trainings.controllers.models;

public enum KlientType {

    FIRMA ("FIRMA"),
    FIZYCZNA ("OSOBA FIZYCZNA"),
    TAXFREE ("TAX FREE");

    private final String name;

    KlientType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
