package pl.eo.trainings.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.eo.trainings.controllers.models.Towar;
import pl.eo.trainings.controllers.models.TowarType;
import pl.eo.trainings.controllers.models.data.TowarDao;

import javax.validation.Valid;
import java.util.ArrayList;

@Controller
@RequestMapping("towar")
public class TowarController {

    @Autowired
    private TowarDao towarDao;

    static ArrayList<String> towary = new ArrayList<>();

    @RequestMapping(value = "")
    public String index(Model model) {

        model.addAttribute("towary", towarDao.findAll());
        model.addAttribute("title", "Moj Towar");
        return "towar/index";
    }
    //FORMULARZ ROZPOCZYNA SIE TUTAJ
    @RequestMapping(value = "dodajTowar", method = RequestMethod.GET)
    public String wyswietlFormularzDodawaniaTowaru(Model model) {
        model.addAttribute("title", "Dodaj Towary");
        model.addAttribute(new Towar());
        model.addAttribute("towarTypes", TowarType.values());
        return "towar/dodajTowar";

    }
    //PRZETWARZANIE FORMULARZA
    @RequestMapping(value = "dodajTowar", method = RequestMethod.POST)
    public String przetworzFormularzDodawaniaTowaru(@ModelAttribute @Valid Towar newTowar,
                                                    Errors errors, Model model) {

        if (errors.hasErrors()) {
            model.addAttribute("title", "Dodaj Towar");
            return "towar/dodajTowar";
        }

        towarDao.save(newTowar);
        return "redirect:";

    }

    @RequestMapping(value = "usunTowar", method = RequestMethod.GET)
    public String displayRemoveTowarForm(Model model) {
        model.addAttribute("towary", towarDao.findAll());
        model.addAttribute("title", "Usuń Towar");
        return "towar/usunTowar";
    }

    @RequestMapping(value = "usunTowar", method = RequestMethod.POST)
    public String processRemoveTowarForm(@RequestParam int[] towarIds) {

        for (int towarId : towarIds) {
            towarDao.delete(towarId);
        }

        return "redirect:";
    }

}
