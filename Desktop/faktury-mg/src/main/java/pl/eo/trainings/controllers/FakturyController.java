package pl.eo.trainings.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.eo.trainings.controllers.models.Faktura;
import pl.eo.trainings.controllers.models.FakturaType;
import pl.eo.trainings.controllers.models.data.FakturaDao;

import javax.validation.Valid;
import java.util.ArrayList;

@Controller
@RequestMapping("faktura")
public class FakturyController {

    @Autowired
    private FakturaDao fakturaDao;

    static ArrayList<String> faktury = new ArrayList<>();

    @RequestMapping(value = "")
    public String index(Model model) {

        model.addAttribute("faktury", fakturaDao.findAll());
        model.addAttribute("title", "Moje Faktury");

        return "faktura/index";
    }
    //FORMULARZ ROZPOCZYNA SIE TUTAJ
    @RequestMapping(value = "dodaj", method = RequestMethod.GET)
    public String wyswietlFormularzDodawaniaFaktur(Model model) {
        model.addAttribute("title", "Dodaj Fakturę");
        model.addAttribute(new Faktura());
        model.addAttribute("fakturaTypes", FakturaType.values());
        return "faktura/dodaj";

    }
    //PRZETWARZANIE FORMULARZA
    @RequestMapping(value = "dodaj", method = RequestMethod.POST)
    public String przetworzFormularzDodawaniaFaktur(@ModelAttribute @Valid Faktura newFaktura,
                                                    Errors errors, Model model) {

        if (errors.hasErrors()) {
            model.addAttribute("title", "Dodaj Fakturę");
            return "faktury/dodaj";
        }

        fakturaDao.save(newFaktura);
        return "redirect:";

    }

    @RequestMapping(value = "usun", method = RequestMethod.GET)
    public String displayRemoveInvoiceForm(Model model) {
        model.addAttribute("faktury", fakturaDao.findAll());
        model.addAttribute("title", "Usuń Fakturę");
        return "faktura/usun";
    }

    @RequestMapping(value = "usun", method = RequestMethod.POST)
    public String processRemoveInvoiceForm(@RequestParam int[] fakturaIds) {

        for (int fakturaId : fakturaIds) {
            fakturaDao.delete(fakturaId);
        }

        return "redirect:";
    }

}
