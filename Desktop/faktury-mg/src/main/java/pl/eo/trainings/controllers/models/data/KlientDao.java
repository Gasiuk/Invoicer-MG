package pl.eo.trainings.controllers.models.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.eo.trainings.controllers.models.Klient;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface KlientDao extends CrudRepository<Klient, Integer> {
}
