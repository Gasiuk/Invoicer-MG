package pl.eo.trainings.controllers.models;

public enum FakturaType {

    VAT ("VAT"),
    PARAGON ("PARAGON"),
    FIZYCZNA ("OSOBA FIZYCZNA");

    private final String name;

    FakturaType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
