package pl.eo.trainings.controllers.models.form;

import pl.eo.trainings.controllers.models.Cennik;
import pl.eo.trainings.controllers.models.Menu;

import javax.validation.constraints.NotNull;

public class AddMenuItemForm {

	@NotNull
	private int menuId;

	@NotNull
	private int cennikId;

	private Iterable<Cennik> cenniki;

	private Menu menu;

	public AddMenuItemForm() { }

	public AddMenuItemForm(Iterable<Cennik> cenniki, Menu menu) {
		this.cenniki = cenniki;
		this.menu = menu;
	}

	public int getMenuId() {
		return menuId;
	}

	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}

	public void setCennikId(int cennikId) {
		this.cennikId = cennikId;
	}

	public Iterable<Cennik> getCenniki() {
		return cenniki;
	}

	public void setCenniki(Iterable<Cennik> cenniki) {
		this.cenniki = cenniki;
	}

	public Menu getMenu() {
		return menu;
	}

	public int getCennikId() {
		return cennikId;
	}
}
